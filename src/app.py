from python_slurm import app
from python_slurm.models import sshClient
import atexit

def correct_exit():
	sshClient.close()

atexit.register(correct_exit)

if __name__ == '__main__':
	app.run(host=app.config['LISTEN_HOST'], port=app.config['LISTEN_PORT'])
