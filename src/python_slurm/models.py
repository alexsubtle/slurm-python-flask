# -*- coding: utf-8 -*-
import sys
import subprocess
import json

import time
import datetime
import re
import collections
from datetime import timedelta
import paramiko

reload(sys)
sys.setdefaultencoding('utf8')

HOST = '82.196.66.9'
USER = 'webslurm'
SECRET = 'webslurm'

ACTIVE_NODE_STATES = [
    'IDLE', 'ALLOCATED', 'ALLOCATED+',
    'COMPLETING', 'MIXED'
    ]

FAIL_JOB_STATES = [
    'BOOT_FAIL', 'CANCELLED', 'FAILED',
    'NODE_FAIL', 'PREEMPTED', 'TIMEOUT'
    ]

SCONTROL_NODE_STATE = 17
SCONTROL_NODE_NAME = 0
SCONTROL_NODE_BOOT_TIME = 22
SCONTROL_NODE_CPUS = 5
SCONTROL_NODE_CPU_LOAD = 6
SCONTROL_NODE_REAL_MEMORY = 13
SCONTROL_NODE_DISK_MEMORY = 20

SCONTROL_JOB_ID = 0
SCONTROL_JOB_NAME = 1
SCONTROL_JOB_USER = 2
SCONTROL_JOB_JOB_STATE = 8
SCONTROL_JOB_EXIT_CODE = 14
SCONTROL_JOB_ELAPSED = 16
SCONTROL_JOB_TIME_LIMIT = 17
SCONTROL_JOB_START_TIME = 21
SCONTROL_RUNNING_JOB_ALLOC_NODE = 30
SCONTROL_PENDING_JOB_ALLOC_NODE = 30
SCONTROL_RUNNING_JOB_NUM_NODES = 32
SCONTROL_PENDING_JOB_NUM_NODES = 31
SCONTROL_RUNNING_JOB_NUM_CPUS = 33
SCONTROL_PENDING_JOB_NUM_CPUS = 32
SCONTROL_JOB_REVERSE_WORK_DIR = 4
SCONTROL_JOB_REVERSE_STD_ERR = 3
SCONTROL_JOB_REVERSE_STD_OUT = 1

SACCT_FORMAT = "'JobId,JobName,User,Elapsed,Start,State,NodeList,NNodes,NCPUS,ExitCode,MaxDiskWrite,TimeLimit,MaxVMSize,CPUTimeRaw,AllocCPUS,Submit'"

SACCT_JOB_ID = 0
SACCT_JOB_NAME = 1
SACCT_JOB_USER = 2
SACCT_JOB_ELAPSED = 3
SACCT_JOB_START = 4
SACCT_JOB_STATE = 5
SACCT_JOB_NODE_LIST = 6 
SACCT_JOB_NNODES = 7
SACCT_JOB_NCPUS = 8
SACCT_JOB_EXIT_CODE = 9
SACCT_JOB_MAX_DISK_WRITE = 10
SACCT_JOB_TIME_LIMIT = 11
SACCT_JOB_MAX_VMSIZE = 12
SACCT_JOB_CPU_TIME_RAW = 13
SACCT_JOB_ALLOC_CPUS = 14
SACCT_JOB_SUBMIT_TIME = 15

SQUEUE_FORMAT = "'%i|%j|%u|%T|%S|%N|%D|%C'"

SQUEUE_JOB_ID = 0
SQUEUE_JOB_NAME = 1
SQUEUE_JOB_USER = 2
SQUEUE_JOB_STATE = 3
SQUEUE_JOB_START_TIME = 4
SQUEUE_JOB_NODE_LIST = 5
SQUEUE_JOB_NUM_NODES = 6
SQUEUE_JOB_NUM_CPUS = 7

SACCTMGR_NAME = 0
SACCTMGR_ACCOUNT = 1
SACCTMGR_RIGHT = 2

sshClient = paramiko.SSHClient()

def open_ssh():
    sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    sshClient.connect(hostname=HOST, username=USER, password=SECRET, look_for_keys=False)
    print("connected successfully...")

# open_ssh()

class User:
    jobs_run_time_to_cpus = None
    cur_jobs = None
    jobs = None
    name = None
    lastlogin = None
    num_jobs = 0
    cpus_time = 0
    min_nodes = None
    max_nodes = None
    max_job_time = 0

class ClusterStat:
    name = None
    num_jobs = 0
    cpus_time = None
    cores = None
    min_nodes = None
    max_nodes = None
    max_job_time = None    
    users = None

TIME_INTERVAL = 30

def get_cluster_basic_stat():
    stdin, stdout, stderr = sshClient.exec_command("sacctmgr -P -n list user")
    cmd_res = stdout.read()
    total_users = len(cmd_res.split('\n')) - 1
        
    active_nodes = 0
    stdin, stdout, stderr = sshClient.exec_command("scontrol show node -o")
    cmd_res = stdout.read()
    nodes = cmd_res.split('\n')
    for node in nodes:
        if node != "":
            cur_node = node.split(' ')
            if cur_node[SCONTROL_NODE_STATE].split('=')[1] in ACTIVE_NODE_STATES:
                active_nodes += 1
    active_nodes = float(active_nodes) / (len(nodes) - 1) * 100

    stdin, stdout, stderr = sshClient.exec_command("scontrol show job -o")
    cmd_res = stdout.read()
    result = cmd_res
    if result == "No jobs in the system\n":
        active_jobs = 0
    else:
        active_jobs = len(result.split('\n')) - 1

    cluster_stat = {u"total_users" : total_users,
            u"active_nodes" : active_nodes,
            u"active_jobs" : active_jobs}
    return cluster_stat

def get_cluster_summary():
    cluster_summary = ClusterStat()
    cluster_summary.users = dict()
    cluster_summary.cores = get_num_cluster_cores();
    stdin, stdout, stderr = sshClient.exec_command("sacctmgr show cluster -P -n")
    cmd_res = stdout.read()
    cluster_summary.name = cmd_res.split("|")[0]
    start_time = (datetime.datetime.now() + timedelta(days=-TIME_INTERVAL)).strftime("%Y-%m-%d")
    stdin, stdout, stderr = sshClient.exec_command("sacct -P -n -a -o " + SACCT_FORMAT + " -S " + start_time)
    cmd_res = stdout.read()
    result = cmd_res

    cluster_summary.cpus_time = 0
    for job in result.split('\n'):
        cur_job = job.split('|')
        if (cur_job[SACCT_JOB_ID] != ''):
            if (not '.' in cur_job[SACCT_JOB_ID]):
                cluster_summary.num_jobs += 1
                cluster_summary.min_nodes = min(cluster_summary.min_nodes if cluster_summary.min_nodes is not None else float('inf'), int(cur_job[SACCT_JOB_ALLOC_CPUS]))
                cluster_summary.max_nodes = max(cluster_summary.max_nodes if cluster_summary.max_nodes is not None else float('-inf'), int(cur_job[SACCT_JOB_ALLOC_CPUS]))
                if cur_job[SACCT_JOB_USER] not in cluster_summary.users:
                    cluster_summary.users[cur_job[SACCT_JOB_USER]] = {
                        u"last_submit" : None,
                        u"num_jobs" : 0
                    }
                cluster_summary.users[cur_job[SACCT_JOB_USER]]['last_submit'] = max(cluster_summary.users[cur_job[SACCT_JOB_USER]]['last_submit'], cur_job[SACCT_JOB_SUBMIT_TIME])
                cluster_summary.users[cur_job[SACCT_JOB_USER]]['num_jobs'] += 1
                if int(cur_job[SACCT_JOB_ALLOC_CPUS]) != 0:
                    cluster_summary.cpus_time = cluster_summary.cpus_time + int(cur_job[SACCT_JOB_CPU_TIME_RAW]) / cluster_summary.cores
                    cluster_summary.max_job_time = max(cluster_summary.max_job_time, int(cur_job[SACCT_JOB_CPU_TIME_RAW]) / cluster_summary.cores)
    cluster_summary.cpus_time = str(datetime.timedelta(seconds=cluster_summary.cpus_time))
    cluster_summary.max_job_time = str(datetime.timedelta(seconds=cluster_summary.max_job_time))
    for user in cluster_summary.users:
        last_submit = cluster_summary.users[user]['last_submit'].split('T')
        last_submit = last_submit[0] + ' ' + last_submit[0]
        cluster_summary.users[user]['last_submit'] = last_submit
    return cluster_summary

def get_nodes_info():
    nodes_info = dict()
    stdin, stdout, stderr = sshClient.exec_command("scontrol show node -o")
    cmd_res = stdout.read()
    result = cmd_res
    for node in result.split('\n'):
        if node != "":
            cur_node = node.split(' ')
            boot_time = cur_node[SCONTROL_NODE_BOOT_TIME].split('=')[1].split('T')
            boot_time = boot_time[0] + ' ' + boot_time[1]
            nodes_info[cur_node[SCONTROL_NODE_NAME].split('=')[1]] = {
                            u"name" : cur_node[SCONTROL_NODE_NAME].split('=')[1],
                            u"state" : cur_node[SCONTROL_NODE_STATE].split('=')[1],
                            u"boot_time" : boot_time,
                            u"cpus" : cur_node[SCONTROL_NODE_CPUS].split('=')[1],
                            u"cpu_load" : float(cur_node[SCONTROL_NODE_CPU_LOAD].split('=')[1]),
                            u"real_memory" : cur_node[SCONTROL_NODE_REAL_MEMORY].split('=')[1],
                            u"disk_memory" : cur_node[SCONTROL_NODE_DISK_MEMORY].split('=')[1]
                            }
    return nodes_info

def get_users_info():
    users_info = dict()
    stdin, stdout, stderr = sshClient.exec_command("sacctmgr -P -n list user")
    cmd_res = stdout.read()
    result = cmd_res
    for user in result.split('\n'):
        if user != "":
            cur_user = user.split('|')
            users_info[cur_user[SACCTMGR_NAME]] = { u"name" : cur_user[SACCTMGR_NAME],
                            u"account" : cur_user[SACCTMGR_ACCOUNT],
                            u"administrator" : cur_user[SACCTMGR_RIGHT]
                            }
    return users_info

def get_user_info(user):
    user_info = User()
    user_info.name = user

    stdin, stdout, stderr = sshClient.exec_command("lastlog -u " + user + " | tail -n 1 | awk '{$1=\"\";$2=\"\";$3=\"\";print $0 }'")
    cmd_res = stdout.read()
    user_info.last_login = cmd_res
    user_info.cur_jobs = []
    user_info.jobs_run_time_to_cpus = []
    try:
        stdin, stdout, stderr = sshClient.exec_command("sacct -s R,PD -P -n -u " + user)
        cmd_res = stdout.read()
        result = cmd_res
    except subprocess.CalledProcessError:
        result = ""
    for job in result.split('\n'):
        user_info.cur_jobs.append(job.split('|'))
    user_info.cur_jobs.pop()

    start_time = (datetime.datetime.now() + timedelta(days=-TIME_INTERVAL)).strftime("%Y-%m-%d")
    # CPUTimeRaw возвращает время всех ядер. Время задачи = CPUTimeRaw / CPUS
    try:
        stdin, stdout, stderr = sshClient.exec_command("sacct -P -n -o " + SACCT_FORMAT + " -S " + start_time + " -u " + user)
        cmd_res = stdout.read()
        result = cmd_res
    except subprocess.CalledProcessError:
        return user_info
    num_cluster_cores = get_num_cluster_cores()
    for job in result.split('\n'):
        cur_job = job.split('|')
        if (cur_job[SACCT_JOB_ID] != ''):
            if (not '.' in cur_job[SACCT_JOB_ID]):
                user_info.num_jobs += 1
                user_info.jobs_run_time_to_cpus.append([cur_job[SACCT_JOB_CPU_TIME_RAW],cur_job[SACCT_JOB_ALLOC_CPUS]])
                user_info.min_nodes = min(user_info.min_nodes if user_info.min_nodes is not None else float('inf'), int(cur_job[SACCT_JOB_ALLOC_CPUS]))
                user_info.max_nodes = max(user_info.max_nodes if user_info.max_nodes is not None else float('-inf'), int(cur_job[SACCT_JOB_ALLOC_CPUS]))
                if int(cur_job[SACCT_JOB_ALLOC_CPUS]) != 0:
                    user_info.cpus_time = user_info.cpus_time + int(cur_job[SACCT_JOB_CPU_TIME_RAW]) / num_cluster_cores
                    user_info.max_job_time = max(user_info.max_job_time, int(cur_job[SACCT_JOB_CPU_TIME_RAW]) / num_cluster_cores)
    user_info.cpus_time = str(datetime.timedelta(seconds=user_info.cpus_time))
    user_info.max_job_time = str(datetime.timedelta(seconds=user_info.max_job_time))

    user_info.jobs_run_time_to_cpus = json.dumps(user_info.jobs_run_time_to_cpus)
    return user_info

def elapsed_time_for_chart(user):
    data = []
    start_time = (datetime.datetime.now() + timedelta(days=-TIME_INTERVAL)).strftime("%Y-%m-%d")
    stdin, stdout, stderr = sshClient.exec_command("sacct -P -n -o JobId,CPUTimeRaw,AllocCPUS -S " + start_time + " -u " + user)
    cmd_res = stdout.read()
    result = cmd_res
    # cur_user.cpus_time = datetime.timedelta(0)
    for job in result.split('\n'):
        cur_job = job.split('|')
        if (cur_job[0] != ''):
            if (not '.' in cur_job[0]):
                if int(cur_job[2]) != 0:
                    data.append([int(cur_job[1]) / int(cur_job[2]),cur_job[2]])
    jsdata = {u"label" : "Elapsed time", u"data": data}
    return json.dumps(jsdata)

def cluster_load_for_chart():
    cluster_load = 0
    nodes_cnt = 0
    stdin, stdout, stderr = sshClient.exec_command("scontrol show node -o")
    cmd_res = stdout.read()
    nodes_info = cmd_res
    for node in nodes_info.split('\n'):
        if node != "":
            cur_node = node.split(' ')
            cluster_load += float(cur_node[SCONTROL_NODE_CPU_LOAD].split('=')[1])
            nodes_cnt += 1
    cluster_load /= nodes_cnt

    jsdata = {u"label" : "Cluster load", u"data": cluster_load}
    return json.dumps(jsdata)

def get_num_cluster_cores():
    num_cores = 0
    stdin, stdout, stderr = sshClient.exec_command("scontrol show node -o")
    cmd_res = stdout.read()
    nodes_info = cmd_res
    nodes_info = nodes_info.split('\n')
    nodes_info.pop()
    for node in nodes_info:
            cur_node = node.split(' ')
            num_cores += int(cur_node[SCONTROL_NODE_CPUS].split('=')[1])
    return num_cores

def parse_nodes_names(nodes):
    parsed_nodes = list()
    if '[' not in nodes:
        parsed_nodes.append(nodes)
    else:
        nodes = nodes.split('[')
        nodes[1] = nodes[1].split('-')
        nodes[1][1] = nodes[1][1].split(']')[0]
        l = len(nodes[1][0])
        for node_index in range(int(nodes[1][0]), 1 + int(nodes[1][1])):
            parsed_nodes.append(nodes[0] + "%0*d" % (l, node_index))
    return parsed_nodes

def parse_cores(cpus):
    cores = list()
    cpus = cpus.split(',')
    for cpu in cpus:
        if '-' not in cpu:
            cores.append(int(cpu))
        else:
            cpu = cpu.split('-')
            for core in range(int(cpu[0]), 1 + int(cpu[1])):
                cores.append(core)
    return cores

def jobs_placement(jobs):
    placement = {u"nodes" : dict(),
                u"max_cpus" : None}
    stdin, stdout, stderr = sshClient.exec_command("scontrol show node -o")
    cmd_res = stdout.read()
    result = cmd_res
    for node in result.split('\n'):
        if node != "":
            cur_node = node.split(' ')
            if cur_node[SCONTROL_NODE_STATE].split('=')[1] not in ACTIVE_NODE_STATES:
                cpus = ["Down"]*int(cur_node[SCONTROL_NODE_CPUS].split('=')[1])
            else:
                cpus = ["Free"]*int(cur_node[SCONTROL_NODE_CPUS].split('=')[1])
            node_name = cur_node[SCONTROL_NODE_NAME].split('=')[1]
            placement['nodes'][node_name] = {
                            u"name" : node_name,
                            u"cpus" : cpus
                            }
            placement['max_cpus'] = max (placement['max_cpus'] if placement['max_cpus'] is not None else float('-inf'), int(cur_node[SCONTROL_NODE_CPUS].split('=')[1]))
    

    for job in jobs:
        for alloc_nodes in jobs[job]['placement']:
            cur_job = alloc_nodes.split(' ')
            nodes = cur_job[0].split('=')[1]
            cpus = cur_job[1].split('=')[1]

            nodes = parse_nodes_names(nodes)
            cores = parse_cores(cpus)

            for node in nodes:
                for core in cores:
                    if placement['nodes'][node]['cpus'][core] == 'Free':
                        placement['nodes'][node]['cpus'][core] = list()
                    placement['nodes'][node]['cpus'][core].append(int(job))

    placement['nodes'] = collections.OrderedDict(sorted(placement['nodes'].items()))
    return placement

def get_current_jobs_info():
    jobs = dict()
    stdin, stdout, stderr = sshClient.exec_command("scontrol show -d job -o")
    cmd_res = stdout.read()
    result = cmd_res
    if result != "No jobs in the system\n":
        for job in result.split('\n'):
            if job != "":
                cur_job = job.split(' ')
                for attribute in cur_job:
                    if '=' not in attribute or attribute == "":
                        # cur_job[attribute - 1] += str(cur_job[attribute])
                        cur_job.remove(attribute)
                # print job
                placement = re.findall(r'Nodes=\S+ CPU_IDs=\S+',job)
                l = len(cur_job) 
                try:
                    std_err = cur_job[l - SCONTROL_JOB_REVERSE_STD_ERR].split('=')[1]
                except IndexError:
                    std_err = ""
                try:
                    std_out = cur_job[l - SCONTROL_JOB_REVERSE_STD_OUT].split('=')[1]
                except IndexError:
                    std_out = ""
                progress = "N/A"

                start_time = datetime.datetime.strptime(cur_job[21].split('=')[1], "%Y-%m-%dT%H:%M:%S")
                time_limit_str = cur_job[SCONTROL_JOB_TIME_LIMIT].split('=')[1]
                if '-' in time_limit_str:
                    time_limit = datetime.datetime.strptime(time_limit_str, "%d-%H:%M:%S")
                else:
                    time_limit = datetime.datetime.strptime(time_limit_str, "%H:%M:%S")

                time_limit_in_seconds = datetime.timedelta(days=time_limit.day, hours=time_limit.hour, minutes=time_limit.minute, seconds=time_limit.second).total_seconds()
                elapsed_time_in_seconds = (datetime.datetime.now() - start_time).total_seconds()
                progress = int (elapsed_time_in_seconds / time_limit_in_seconds * 100)

                if cur_job[SCONTROL_JOB_JOB_STATE].split('=')[1] == "RUNNING":
                    num_nodes = cur_job[SCONTROL_RUNNING_JOB_NUM_NODES].split('=')[1]
                    num_cpus = cur_job[SCONTROL_RUNNING_JOB_NUM_CPUS].split('=')[1]
                    alloc_nodes = cur_job[SCONTROL_RUNNING_JOB_ALLOC_NODE].split('=')[1]
                else:
                    num_nodes = cur_job[SCONTROL_PENDING_JOB_NUM_NODES].split('=')[1]
                    num_cpus = cur_job[SCONTROL_PENDING_JOB_NUM_CPUS].split('=')[1]
                    alloc_nodes = cur_job[SCONTROL_PENDING_JOB_ALLOC_NODE].split('=')[1]
                
                jobs[cur_job[SCONTROL_JOB_ID].split('=')[1]] = {
                            u"job_id" : cur_job[SCONTROL_JOB_ID].split('=')[1],
                            u"name" : cur_job[SCONTROL_JOB_NAME].split('=')[1],
                            u"user_id" : cur_job[SCONTROL_JOB_USER].split('=')[1],
                            u"run_time_str" : cur_job[SCONTROL_JOB_ELAPSED].split('=')[1],
                            u"progress" : progress,
                            u"start_time" : start_time,
                            u"job_state" : cur_job[SCONTROL_JOB_JOB_STATE].split('=')[1],
                            u"alloc_node" : alloc_nodes,
                            u"num_nodes" : num_nodes,
                            u"num_cpus" : num_cpus,
                            u"work_dir" : cur_job[l - SCONTROL_JOB_REVERSE_WORK_DIR].split('=')[1],
                            u"exit_code" : cur_job[SCONTROL_JOB_EXIT_CODE].split('=')[1],
                            u"std_err" : std_err,
                            u"std_out" : std_out,
                            u"placement" : placement}
    return jobs

def get_jobs_in_interval_info(start_date, end_date):
    cmd = "sacct -P -n -a -o " + SACCT_FORMAT
    if start_date:
        cmd += " -S " + start_date
    if end_date:
        cmd += " -E " + end_date

    jobs = dict()
    stdin, stdout, stderr = sshClient.exec_command(cmd)
    cmd_res = stdout.read()
    result = cmd_res
    for job in result.split('\n'):
        cur_job = job.split('|')
        if (cur_job[SACCT_JOB_ID] != ''):
            if (not '.' in cur_job[SACCT_JOB_ID]):
                if cur_job[SACCT_JOB_STATE] == "RUNNING":
                    start_time = datetime.datetime.strptime(cur_job[SACCT_JOB_START], "%Y-%m-%dT%H:%M:%S")
                    if '-' in cur_job[SACCT_JOB_TIME_LIMIT]:
                        tl = datetime.datetime.strptime(cur_job[SACCT_JOB_TIME_LIMIT], "%d-%H:%M:%S")
                    else:
                        tl = datetime.datetime.strptime(cur_job[SACCT_JOB_TIME_LIMIT], "%H:%M:%S")

                    tl_in_seconds = datetime.timedelta(days=tl.day, hours=tl.hour, minutes=tl.minute, seconds=tl.second).total_seconds()
                    elapsed_time_in_seconds = (datetime.datetime.now() - start_time).total_seconds()
                    progress = int (elapsed_time_in_seconds / tl_in_seconds * 100)
                else:
                    progress = 100

                if cur_job[SACCT_JOB_STATE].split(' ')[0] in FAIL_JOB_STATES:
                    failure = True
                else:
                    failure = False
                if cur_job[SACCT_JOB_START] != 'Unknown':
                    start_time = cur_job[SACCT_JOB_START].split('T')
                    start_time = start_time[0] + ' ' + start_time[1]
                else:
                    start_time = u"Unknown"

                jobs[cur_job[SACCT_JOB_ID]] = {
                    u"job_id" : cur_job[SACCT_JOB_ID],
                    u"name" : cur_job[SACCT_JOB_NAME],
                    u"user_id" : cur_job[SACCT_JOB_USER],
                    u"run_time_str" : cur_job[SACCT_JOB_ELAPSED],
                    u"progress" : progress,
                    u"start_time" : start_time,
                    u"job_state" : cur_job[SACCT_JOB_STATE],
                    u"failure" : failure,
                    u"alloc_node" : cur_job[SACCT_JOB_NODE_LIST],
                    u"num_nodes" : cur_job[SACCT_JOB_NNODES],
                    u"num_cpus" : cur_job[SACCT_JOB_NCPUS],
                    u"work_dir" : "N/A",
                    u"exit_code" : cur_job[SACCT_JOB_EXIT_CODE],
                    u"tmp_disk" : cur_job[SACCT_JOB_MAX_DISK_WRITE],
                    u"std_err" : "N/A",
                    u"std_out" : "N/A"}
    return jobs

def get_node_info(node):
    cmd = "scontrol show node -o " + node
    try:
        stdin, stdout, stderr = sshClient.exec_command(cmd)
        cmd_res = stdout.read()
        result = cmd_res
        if not "not found" in result:
            cur_node = result.split(' ')

            return {u"cpu_load" : float(cur_node[SCONTROL_NODE_CPU_LOAD].split('=')[1]),
                    u"up_time" : cur_node[SCONTROL_NODE_BOOT_TIME].split('=')[1],
                    u"state" : cur_node[SCONTROL_NODE_STATE].split('=')[1],
                    u"real_memory" : cur_node[SCONTROL_NODE_REAL_MEMORY].split('=')[1],
                    u"cpus" : cur_node[SCONTROL_NODE_CPUS].split('=')[1]}
    except subprocess.CalledProcessError:
        return None

def get_node_jobs_info(node):
    jobs = dict()
    # id name user state start_time nodelist
    cmd = "squeue -h -o " + SQUEUE_FORMAT + " -w " + node
    try:
        stdin, stdout, stderr = sshClient.exec_command(cmd)
        cmd_res = stdout.read()
        result = cmd_res
        if result != "":
            for job in result.split('\n'):
                if job != "":
                    cur_job = job.split('|')
                    start_time = cur_job[SQUEUE_JOB_START_TIME].split('T')
                    start_time = start_time[0] + ' ' + start_time[1]
                    jobs[cur_job[0]] = {
                        u"job_id" : cur_job[SQUEUE_JOB_ID],
                        u"name" : cur_job[SQUEUE_JOB_NAME],
                        u"user_id" : cur_job[SQUEUE_JOB_USER],
                        u"job_state" : cur_job[SQUEUE_JOB_STATE],
                        u"start_time" : start_time,
                        u"node_list" : cur_job[SQUEUE_JOB_NODE_LIST],
                        u"num_nodes" : cur_job[SQUEUE_JOB_NUM_NODES],
                        u"num_cpus" : cur_job[SQUEUE_JOB_NUM_CPUS],
                        u"exit_code" : "N/A"}
        return jobs
    except subprocess.CalledProcessError:
        return None

def get_node_jobs_in_interval_info(node, start_date, end_date):
    cmd = "sacct -P -n -a -o " + SACCT_FORMAT
    if start_date:
        cmd += " -S " + start_date
    if end_date:
        cmd += " -E " + end_date

    jobs = dict()
    stdin, stdout, stderr = sshClient.exec_command(cmd)
    cmd_res = stdout.read()
    result = cmd_res
    for job in result.split('\n'):
        cur_job = job.split('|')
        if (cur_job[SACCT_JOB_ID] != ''):
            if (not '.' in cur_job[SACCT_JOB_ID]):
                node_list = parse_nodes_names(cur_job[SACCT_JOB_NODE_LIST])
                if node in node_list:
                    start_time = cur_job[SACCT_JOB_START].split('T')
                    start_time = start_time[0] + ' ' + start_time[1]
                    jobs[cur_job[SACCT_JOB_ID]] = {
                        u"job_id" : cur_job[SACCT_JOB_ID],
                        u"name" : cur_job[SACCT_JOB_NAME],
                        u"user_id" : cur_job[SACCT_JOB_USER],
                        u"start_time" : start_time,
                        u"node_list" : cur_job[SACCT_JOB_NODE_LIST],
                        u"job_state" : cur_job[SACCT_JOB_STATE],
                        u"alloc_node" : cur_job[SACCT_JOB_NODE_LIST],
                        u"num_nodes" : cur_job[SACCT_JOB_NNODES],
                        u"num_cpus" : cur_job[SACCT_JOB_NCPUS],
                        u"exit_code" : cur_job[SACCT_JOB_EXIT_CODE]
                    }
    return jobs

def get_job_info(job_id):
    cmd = "sacct -P -n -a -o " + SACCT_FORMAT + " -j" + job_id
    try:
        job = dict()
        stdin, stdout, stderr = sshClient.exec_command(cmd)
        cmd_res = stdout.read()
        result = cmd_res
        if result != "":
            for job_s in result.split('\n'):
                if job_s != "":
                    job_step = job_s.split('|')
                    if job_step[SACCT_JOB_START] != "Unknown":
                        start_time = job_step[SACCT_JOB_START].split('T')
                        start_time = start_time[0] + ' ' + start_time[1]
                    else:
                        start_time = job_step[SACCT_JOB_START]
                    job[job_step[SACCT_JOB_ID]] = {
                        u"job_id" : job_step[SACCT_JOB_ID],
                        u"name" : job_step[SACCT_JOB_NAME],
                        u"user_id" : job_step[SACCT_JOB_USER],
                        u"run_time_str" : job_step[SACCT_JOB_ELAPSED],
                        u"start_time" : start_time,
                        u"job_state" : job_step[SACCT_JOB_STATE],
                        u"node_list" : job_step[SACCT_JOB_NODE_LIST],
                        u"num_nodes" : job_step[SACCT_JOB_NNODES],
                        u"num_cpus" : job_step[SACCT_JOB_NCPUS],
                        u"exit_code" : job_step[SACCT_JOB_EXIT_CODE],
                        u"disk_write" : job_step[SACCT_JOB_MAX_DISK_WRITE],
                        u"vm_size" : job_step[SACCT_JOB_MAX_VMSIZE]}
    except subprocess.CalledProcessError:
        return None
    return job