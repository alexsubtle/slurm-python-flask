# -*- coding: utf-8 -*-
from python_slurm import app
from .forms import JobsInterval
from .models import get_cluster_basic_stat, get_cluster_summary, get_nodes_info, get_node_info, get_users_info, get_user_info, elapsed_time_for_chart, cluster_load_for_chart, get_current_jobs_info, get_jobs_in_interval_info, get_node_jobs_info, get_node_jobs_in_interval_info, get_job_info, jobs_placement, open_ssh

from flask import render_template, request, url_for, redirect, session, flash, g, Response, render_template_string, stream_with_context
import time
# from werkzeug import secure_filename

@app.before_first_request
def before_first_request():
    open_ssh()

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/cluster', methods=["GET", "POST"])
def cluster():
    return render_template('cluster.html')

@app.route('/nodes', methods=["GET", "POST"])
def nodes():
    return render_template('nodes.html')

@app.route('/users', methods=["GET", "POST"])
def users():
    return render_template('users.html')

@app.route('/user/<user>', methods=["GET", "POST"])
def user(user):
    # сообщение и переход
    flash(u'Пользователь')

    cur_user = get_user_info(user)
    return render_template('user.html', user=cur_user)

@app.route('/jobs', methods=["GET", "POST"])
def jobs():
    # сообщение и переход
    flash(u'Список задач')
    
    form = JobsInterval(request.form)

    jobs = get_current_jobs_info()
    placement = jobs_placement(jobs)

    if request.method == 'POST' and form.validate():
        jobs = get_jobs_in_interval_info(request.form['start_date'], request.form['end_date'])
        return render_template('jobs.html', jobs=jobs, form=form, jobs_placement=placement)

    return render_template('jobs.html', jobs=jobs, form=form, jobs_placement=placement)

@app.route('/node/<node>', methods=["GET", "POST"])
def node(node):
    # сообщение и переход
    flash(u'Вычислительный узел:%s.' % node)

    form = JobsInterval(request.form)
    node_info = get_node_info(node)

    if node_info is not None:
        if request.method == 'POST' and form.validate():
            jobs = get_node_jobs_in_interval_info(node, request.form['start_date'], request.form['end_date'])
            return render_template('/node.html', node = node_info, jobs=jobs, form=form)

        jobs = get_node_jobs_info(node)
        return render_template('/node.html', node = node_info, jobs=jobs, form=form)
    else:
        return render_template('404.html')

@app.route('/job/<job_id>', methods=["GET", "POST"])
def job(job_id):

    flash(u'Задача:%s' % job_id)

    job = get_job_info(job_id)
    if job is not None:
        return render_template('job.html', job=job)
    else:
        return render_template('404.html')

@app.route('/chart-data/user/<user>', methods=["GET"])
def chart_elapsed_time(user):
    return elapsed_time_for_chart(user)

@app.route('/chart-data/cluster/load', methods=["GET"])
def chart_cluster_load():
    return cluster_load_for_chart()

@app.route('/data/dashboard')
def dashboardData():
    dashboard = get_cluster_basic_stat()
    return render_template('dashboard.html', cluster=dashboard)
    
@app.route('/data/cluster')
def clusterData():
    cluster = get_cluster_summary()
    return render_template('clusterData.html', cluster=cluster)

@app.route('/data/users')
def usersData():
    users = get_users_info()
    return render_template('usersData.html', users=users)

@app.route('/data/nodes')
def nodesData():
    nodes = get_nodes_info()
    return render_template('nodesData.html', nodes=nodes)