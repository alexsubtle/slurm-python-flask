# -*- coding: utf-8 -*-
from flask import Flask
import os

app = Flask(__name__)

app.config.from_object('python_slurm.config.DevelConfig')
if 'PYTHON_SLURM_SETTINGS' in os.environ:
    app.config.from_envvar('PYTHON_SLURM_SETTINGS')
    
# db = SQLAlchemy(app)

if not app.debug:
    import logging
    file_handler = logging.FileHandler(app.config["LOG_FILE"])
    file_handler.setLevel(logging.WARNING)

    app.logger.addHandler(file_handler)
    
import python_slurm.controllers