# -*- coding: utf-8 -*-
from flask_wtf import Form
from wtforms import Form, TextField, validators, fields, DateTimeField

class JobsInterval(Form):
  start_date = DateTimeField('Start at', format="%Y-%m-%dT%H:%M", validators=[validators.Optional()])
  end_date = DateTimeField('End at', format="%Y-%m-%dT%H:%M", validators=[validators.Optional()])