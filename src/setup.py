from setuptools import setup, find_packages

setup(
    name='Python slurm flask',
    version='0.1',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    # test_suite="tests.all_tests",
    install_requires=[
        # 'Fabric',
        # 'mysql-python',
        # 'alembic',
        # 'Flask-SQLAlchemy',
        # 'Flask-WTF',
        'flask'
    ]
)
