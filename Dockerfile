FROM tiangolo/uwsgi-nginx-flask:flask

RUN pip install flask-wtf paramiko

COPY nginx.conf /etc/nginx/conf.d/
COPY src /app
